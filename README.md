# JSON:API Links

This modules allows it to remove the "links" attributes from the JSON:API response.

There is a setting located in the JSON:API Settings to turn it on and off.

There is one exception: if you're requesting /jsonapi (or the path you set up for that root-call) nothing will be removed.
