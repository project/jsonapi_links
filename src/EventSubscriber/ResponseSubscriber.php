<?php

namespace Drupal\jsonapi_links\EventSubscriber;

use Drupal\jsonapi\ResourceResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ResponseSubscriber implements EventSubscriberInterface {
  protected array $path = [];
  protected array $ignoreList = [];

  public function onResponse(ResponseEvent $event) {
    $response = $event->getResponse();
    // First we check if we have an JSON:API response
    if (strtolower($response->headers->get('content-type', 'unknown')) !== 'application/vnd.api+json') {
      return;
    }

    $content = $response->getContent();
    $json = json_decode($content);

    // 2nd check if we have an JSON:API JSON
    if (!isset($json->jsonapi)) {
      return;
    }

    // This check shall prevent, that the meta.links.me info is removed.
    $jsonapi_base_path = \Drupal::getContainer()->getParameter('jsonapi.base_path');
    if ($event->getRequest()->getPathInfo() === $jsonapi_base_path) {
      return;
    }

    // Check if we shall do anything at all
    $config = \Drupal::config('jsonapi_links.settings');

    if (!$config->get('remove_links')) {
      return;
    }
    
    if ($config->get('ignore_list')) {
      $this->ignoreList = array_filter(array_map('trim', explode("\n", $config->get('ignore_list'))));
    }

    $this->removeLinks($json);

    $response->setContent(json_encode($json));
    $event->setResponse($response);
  }

  protected function removeLinks(&$data, string $node = null) {
    if ($node) {
      $this->path[] = $node;
    }

    // If we find the current path on the ignore list, we skip anything below that
    if ($this->isOnIgnoreList()) {
      array_pop($this->path);
      return;
    }

    if (is_scalar($data)) {
      array_pop($this->path);
      return;
    }

    if (is_object($data)) {
      // If this is the JSON:API response, do not remove the links.
      // The links on the response may include pager links.
      if (!property_exists($data, 'jsonapi')) {
        // Meta must return an object. If links are the only property,
        // and the links are removed by this module, then an empty array
        // will be returned instead.
        // So, if meta only contains links, then remove meta as well.
        if (property_exists($data, 'meta') && property_exists($data->meta, 'links')) {
          unset($data->meta->links);
          $property_list = get_object_vars($data->meta);
          if (count($property_list) === 0) {
            unset($data->meta);
          }
        }
        unset($data->links);
      }

      foreach (get_object_vars($data) as $name => $attribute) {
        $this->removeLinks($data->$name, $name);
      }

      array_pop($this->path);
      return;
    }

    if (is_array($data)) {
      foreach ($data as $i => &$item) {
        $this->removeLinks($item, $i);
      }
    }

    array_pop($this->path);
  }

  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onResponse', 112];
    return $events;
  }

  private function isOnIgnoreList(): bool {
    $path = implode('.', $this->path);

    for ($i = 0; $i < count($this->ignoreList); $i++) {
      if (preg_match($this->ignoreList[$i], $path)) {
        return true;
      }
    }

    return false;
  }
  
}
