<?php

namespace Drupal\jsonapi_links\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\ProxyClass\Routing\RouteBuilder;
use Drupal\jsonapi\ResourceType\ResourceTypeRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Configure JSON:API settings for this site.
 */
class SettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jsonapi_links_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'jsonapi_links.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $field_type = NULL) {
    $config = $this->config('jsonapi_links.settings');

    $form = [];

    $form['remove_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove all links attributes'),
      '#description' => $this->t('All links attributes will be recursive removed, except for the /jsonapi response.'),
      '#default_value' => $config->get('remove_links'),
    ];

    $form['ignore_list'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Do not touch links, below these attributes'),
      '#description' => $this->t('Because, there are useful *links* sections from other modules, you can define sections which shall be ignored. One per line. Example "/jsonapi\.foo\.bar/". Each line must be a valid PHP RegEx. You can support array structures like so: "/foo\.[0-9]+\.bar/"'),
      '#default_value' => $config->get('ignore_list'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('jsonapi_links.settings');

    $keys = [
      'remove_links',
      'ignore_list',
    ];

    foreach ($keys as $key) {
      $config->set($key, $form_state->getValue($key));
    }

    $config->save();
  }
}
